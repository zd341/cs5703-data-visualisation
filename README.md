# CS5703-DATA VISUALISATION

## Group Project Pipeline for Data Visualisation
------------------------------------------------------------------------------------------------------------------------------------------------------------
**Submission Date: 05/03/2021**

**Weeks on hand: 3 weeks 9 days**

Week 1 
- [x] 1. Select columns to retain and remove. The deliverable (Discussion).
- [ ] 2. Join datasets –> The deliverable.
- [x] 3. Brainstorm Personas 2 per person.
- [x] 4. Brainstorm questions 5 per person.
- [ ] 5. Make a sample out of the main dataset. 

Week 2 
- [ ] 1. Creating metadata and documentation on each column.
- [ ] 2. Loading Data in Tableau. 
- [ ] 3. Mock Proposal for dashboard. 


- [ ] 1. Discuss Improvements to Mock Proposal.
- [ ] 2. Create a two-page report. 

Last 4 days perform last-minute fixes and improvements for our proposal


